<?php

namespace App\Controller;

use Silex\ControllerCollection;
use Symfony\Component\Form\Extension\Core\Type as FormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints;

class MailController extends \Core\AbstractController
{
    public function helloAction( $name )
    {
        return $this->app->render( 'pupik.html.twig' , [ 'name' => $this->app->escape($name) ] );

        return $this->app->trans( 'hlasky.predstav' , [ '%jmeno%' => $name ] ).' '.$this->app['locale'];
        return 'Hello '.$this->app->escape($name) .' '. $this->app['locale'];
    }

    public function formAction( Request $request )
    {
        // jméno, rok nar., pohlaví, zemi

        $genders    =   [ 'Žena' => 'F' , 'Muž' => 'M' , 'Nevím' => 'X' ];

        $fb =   $this->app['form.factory']->createNamedBuilder( 'uzivatel' );

        $fb
        ->add( 'name' , FormType\TextType::class ,
                [
                    'label'         =>  'Jméno' ,
                    'required'      =>  true ,
                    'constraints'   =>
                        [
                            new Constraints\Length([ 'min' => 3 , 'max' => 10 ]) ,
                            new Constraints\NotBlank() ,
                        ]
                ]
        )
        ->add( 'gender' , FormType\ChoiceType::class ,
            [
                'label'         =>  'Pohlaví' ,
                'required'      =>  true ,
                'choices'       =>  $genders ,
                'multiple'      =>  true ,
                'expanded'      =>  true ,
                'constraints'   =>
                    [
                        new Constraints\NotBlank() ,
                        new Constraints\Choice([ 'choices' => $genders , 'multiple' => true ])
                    ]
            ]
        )
        ->add( 'country' , FormType\CountryType::class ,
            [
                'label'         =>  'Země' ,
                'required'      =>  true ,
                'expanded'      =>  false ,
                'constraints'   =>
                    [
                        new Constraints\NotBlank() ,
                        new Constraints\Country()
                    ]
            ]
        )
        ->add( 'birthday' , FormType\DateTimeType::class ,
            [
                'label'         =>  'Datum' ,
                'required'      =>  true ,
                'date_widget'   =>  'choice' ,
                'time_widget'   =>  'single_text' ,
                'years'         =>  [ 2015 , 2016 , 2020 ] ,
                'with_seconds'  =>  true ,
                'constraints'   =>
                    [
                        new Constraints\NotBlank() ,
                        new Constraints\DateTime() ,
                    ]
            ]
        )
        ->add( 'submit' , FormType\SubmitType::class ,
            [
                'label'         =>  'Odeslat' ,
            ]
        )
        ->add( 'email' , FormType\EmailType::class ,
            [
                'label'         =>  'Email' ,
                'required'      =>  true ,
                'constraints'   =>
                    [
                        new Constraints\Email([ 'checkMX' => true ]) ,
                        new Constraints\NotBlank() ,
                    ]
            ]
        )
        ;


        $form   =   $fb->getForm();

        if( 'POST' == $request->getMethod() )
        {
            //pokud je metoda POST, tak dosadíme data a zvalidujeme

            $form->submit( $request->request->get( 'uzivatel' ) );

            if( $form->isValid() )
            {
                // je validní, odešleme email a přesměrujeme

                $message = \Swift_Message::newInstance()
                    ->setSubject('[YourSite] Feedback')
                    ->setFrom(array('noreply@yoursite.com'))
                    ->setTo( $form->get( 'email' )->getData() )
                    ->setBcc( $this->app['mailer_user'] , $this->app['mailer_user'] )
                    ->setBody( $this->app->renderView( 'email.html.twig' , [ 'data' => $form->getData() ] ) , 'text/html');

                $this->app['mailer']->send($message);

                return $this->app->redirect( $this->app->url( 'form' ) );
            }
            else
            {
                // není validní, zobrazíme formulář znovu a vypíšeme chyby
                return $this->app->render( 'form.html.twig' , [ 'uzivatel' => $form->createView() ] );
            }
        }
        else
        {
            return $this->app->render( 'form.html.twig' , [ 'uzivatel' => $form->createView() ] );
        }
    }

    /**
     * @param \Silex\ControllerCollection $controllers
     * @return \Silex\ControllerCollection
     */
    protected function connect( ControllerCollection $controllers )
    {
        $controllers
            ->get( '/hello/{name}' , [ $this , 'helloAction' ] ) //callable
            ->bind( 'hello' )
        ;

        $controllers
            ->match( '/form' , [ $this , 'formAction' ] ) //callable
            ->method( 'GET|POST' )
            ->bind( 'form' )
        ;

        return $controllers;
    }
}