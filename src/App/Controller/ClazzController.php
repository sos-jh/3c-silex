<?php

namespace App\Controller;

use App\Entity\Clazz;
use App\Form\ClazzType;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ClazzController extends \Core\AbstractController
{
    public function cgetAction()
    {
        return $this->app->render( '/class/cget.html.twig' , [ 'classes' => $this->getRepository()->findAll() ] );
    }

    public function cpostAction( Request $request )
    {
        $em     =   $this->app['orm.em'];
        $form   =   $this->app->form( new Clazz() , [ 'em' => $em ] , ClazzType::class )->getForm();

        $form->submit( $request->request->get( 'clazz' ) );

        if( $form->isValid() )
        {
            $class      =   $form->getData();

            //musíme na každém nově přidaném studentovi zavolat Student::setClazz()
            foreach( $class->getStudents() as $student )
            {
                $student->setClazz( $class );
            }

            $em->persist( $class );
            $em->flush();

            return $this->app->redirect( $this->app->url( 'get_classes' ) )
                ->setStatusCode( Response::HTTP_CREATED );
        }
        else
        {
            return $this->app->render( '/class/new.html.twig' , [ 'form' => $form->createView() ] )
                ->setStatusCode( Response::HTTP_BAD_REQUEST );
        }
    }

    public function getAction( $class )
    {
        $class    =   $this->getRepository()->find( $class );

        return $this->app->render( '/class/get.html.twig' , [ 'class' => $class ] );
    }

    public function deleteAction( $class )
    {
        $class    =   $this->getRepository()->find( $class );
        $em       =   $this->app['orm.em'];

        //protože třída může být použita jako foreign key u studentů
        //musíme prvně u těchto studentů nastavit id_class na NULL
        //jinak dojde k foreign key constraint violation a mysql nás nenechá třídu smazat
        //stejného výsledku dosáhneme i pomocí nastavení mysql "foreign key on delete set null"
        $students   =   $class->getStudents()->toArray();
        foreach( $students as $key => $student )
        {
            $student->setClazz( null );
            $em->persist( $student );
        }
        $em->flush();

        $em->remove( $class );
        $em->flush();

        return $this->app->redirect( $this->app->url( 'get_classes' ) );
    }

    public function putAction( $class , Request $request )
    {
        $em         =   $this->app['orm.em'];
        $class      =   $this->getRepository()->find( $class );
        $form       =   $this->app->form( $class , [ 'em' => $em , 'clazz_id' => $class->getId() ] , ClazzType::class )->getForm();

        $form->submit( $request->request->get( 'clazz' ) );

        if( $form->isValid() )
        {
            $class      =   $form->getData();

            //musíme na každém nově přidaném studentovi zavolat Student::setClazz()
            foreach( $class->getStudents() as $student )
            {
                $student->setClazz( $class );
            }

            $em->persist( $class );
            $em->flush();

            return $this->app->redirect( $this->app->url( 'get_classes' ) );
        }
        else
        {
            return $this->app->render( '/class/edit.html.twig' , [ 'form' => $form->createView() ] )
                ->setStatusCode( Response::HTTP_BAD_REQUEST );
        }
    }

    public function newAction()
    {
        $form   =   $this->app->form( new Clazz() , [ 'em' => $this->app['orm.em'] ] , ClazzType::class )->getForm();

        return $this->app->render( '/class/new.html.twig' , [ 'form' => $form->createView() ] );
    }

    public function editAction( $class )
    {
        $class    =   $this->getRepository()->find( $class );
        $form     =   $this->app->form( $class , [ 'em' => $this->app['orm.em'] , 'clazz_id' => $class->getId() ] , ClazzType::class )->getForm();

        return $this->app->render( '/class/edit.html.twig' , [ 'form' => $form->createView() ] );
    }

    public function removeAction( $class )
    {
        $class    =   $this->getRepository()->find( $class );

        return $this->app->render( '/class/remove.html.twig' , [ 'class' => $class ] );
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function getRepository()
    {
        return $this->app['orm.em']->getRepository( 'App\Entity\Clazz' );
    }

    protected function connect( ControllerCollection $controllers )
    {
        //http://www.restapitutorial.com/lessons/httpmethods.html

        $controllers
            ->get( '/' , [ $this , 'cgetAction' ] )
            ->bind( 'get_classes' )
        ;

        $controllers
            ->post( '/' , [ $this , 'cpostAction' ] )
            ->bind( 'post_classes' )
        ;

        $controllers
            ->get( '/new' , [ $this , 'newAction' ] )
            ->bind( 'new_class' )
        ;

        $controllers
            ->get( '/{class}' , [ $this , 'getAction' ] )
            ->bind( 'get_class' )
        ;

        $controllers
            ->put( '/{class}' , [ $this , 'putAction' ] )
            ->bind( 'put_class' )
        ;

        $controllers
            ->delete( '/{class}' , [ $this , 'deleteAction' ] )
            ->bind( 'delete_class' )
        ;

        $controllers
            ->get( '/{class}/edit' , [ $this , 'editAction' ] )
            ->bind( 'edit_class' )
        ;

        $controllers
            ->get( '/{class}/remove' , [ $this , 'removeAction' ] )
            ->bind( 'remove_class' )
        ;

        return $controllers;
    }
}