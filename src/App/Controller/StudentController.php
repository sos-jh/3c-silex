<?php

namespace App\Controller;

use App\Entity\Student;
use App\Form\StudentType;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class StudentController extends \Core\AbstractController
{
    public function cgetAction()
    {
        //TODO: translations, studentInfo

//        $s1 =   $er->findBy([ 'firstName' => 'josef' ] , [ 'lastName' => 'asc' ]);
//        $s1 =   $er->findOneBy([ 'firstName' => 'josef' ]);

        /**
         * @var $em \Doctrine\ORM\EntityManagerInterface
         */
        $em =   $this->app['orm.em'];
        $er =   $em->getRepository( 'App\Entity\Student' );

        return $this->app->render( '/student/cget.html.twig' , [ 'students' => $er->findAll() ] );
    }

    public function cgetFreeAction()
    {
        $er         =   $this->getRepository();
//        $students   =   $er->findBy([ 'Clazz' => null ]);

        $qb         =   $er->createQueryBuilder( 's' );
        $qb
            ->where( 's.Clazz IS NULL' )
            ->orderBy( 's.lastName' , 'asc' )
//            ->where( "s.firstName = 'karel'" )
//            ->where( $qb->expr()->eq( 's.firstName' , $qb->expr()->literal( 'karel' ) ) )
        ;

        $students   =   $qb->getQuery()->getResult();

        return $this->app->render( '/student/cget.html.twig' , [ 'students' => $students ] );
    }

    public function cpostAction( Request $request )
    {
        $em     =   $this->app['orm.em'];
        $form   =   $this->app->form( new Student() , [ 'em' => $em ] , StudentType::class )->getForm();

        $form->submit( $request->request->get( 'student' ) );

        if( $form->isValid() )
        {
            $student    =   $form->getData();
            $info       =   $form->get( 'StudentInfo' )->getData();

            //protože StudentInfo je identifying relation, potřebuje ID Studenta
            //ten ho ale ještě nemá, není uložený
            //musíme prvně Student::$StudentInfo nastavit na null,
            //uložit studenta a tím vytvořit jeho ID
            //a pak vycucnout ID studenta přes EntityManager::refresh()
            //poté už nastavíme Student::$StudentInfo i StudentInfo::$Student
            //a uložíme podruhé

            if( $info )
            {
                $student->setStudentInfo( null );
                $info->setStudent( $student );

                $em->persist( $student );
                $em->flush();
                $em->refresh( $student );

                $student->setStudentInfo( $info );
                $info->setStudent( $student );

                $em->persist( $info );
            }

            $em->persist( $student );
            $em->flush();

            return $this->app->redirect( $this->app->url( 'get_students' ) )
                ->setStatusCode( Response::HTTP_CREATED );
        }
        else
        {
            return $this->app->render( '/student/new.html.twig' , [ 'form' => $form->createView() ] )
                ->setStatusCode( Response::HTTP_BAD_REQUEST );
        }
    }

    public function getAction( $student )
    {
        $student    =   $this->getRepository()->find( $student );

        return $this->app->render( '/student/get.html.twig' , [ 'student' => $student ] );
    }

    public function deleteAction( $student )
    {
        $student    =   $this->getRepository()->find( $student );
        $em         =   $this->app['orm.em'];

        if( $student->getStudentInfo() )
        {
            $em->remove( $student->getStudentInfo() );
        }

        $em->remove( $student );
        $em->flush();

        return $this->app->redirect( $this->app->url( 'get_students' ) );
    }

    public function putAction( $student , Request $request )
    {
        $em         =   $this->app['orm.em'];
        $student    =   $this->getRepository()->find( $student );
        $form       =   $this->app->form( $student , [ 'em' => $em ] , StudentType::class )->getForm();

        $form->submit( $request->request->get( 'student' ) );

        if( $form->isValid() )
        {
            $student    =   $form->getData();
            $info       =   $form->get( 'StudentInfo' )->getData();

            if( $info )
            {
                $student->setStudentInfo( $info );
                $info->setStudent( $student );
                $em->persist( $info );
            }

            $em->persist( $student ); //$form::getData() vrací instanci Student
            $em->flush();

            return $this->app->redirect( $this->app->url( 'get_students' ) );
        }
        else
        {
            return $this->app->render( '/student/edit.html.twig' , [ 'form' => $form->createView() ] )
                ->setStatusCode( Response::HTTP_BAD_REQUEST );
        }
    }

    public function newAction()
    {
        $form   =   $this->app->form( new Student() , [ 'em' => $this->app['orm.em'] ] , StudentType::class )->getForm();

        return $this->app->render( '/student/new.html.twig' , [ 'form' => $form->createView() ] );
    }

    public function editAction( $student )
    {
        $student    =   $this->getRepository()->find( $student );
        $form       =   $this->app->form( $student , [ 'em' => $this->app['orm.em'] ] , StudentType::class )->getForm();

        return $this->app->render( '/student/edit.html.twig' , [ 'form' => $form->createView() ] );
    }

    public function removeAction( $student )
    {
        $student    =   $this->getRepository()->find( $student );

        return $this->app->render( '/student/remove.html.twig' , [ 'student' => $student ] );
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function getRepository()
    {
        return $this->app['orm.em']->getRepository( 'App\Entity\Student' );
    }

    protected function createForm()
    {
        //sestavíme fb
        //vrátíme fb nebo form
        return $this->app->form( new Student() , [] , StudentType::class );
    }

    protected function connect( ControllerCollection $controllers )
    {
        //http://www.restapitutorial.com/lessons/httpmethods.html

        $controllers
            ->get( '/' , [ $this , 'cgetAction' ] )
            ->bind( 'get_students' )
        ;

        $controllers
            ->get( '/free' , [ $this , 'cgetFreeAction' ] )
            ->bind( 'get_free_students' )
        ;

        $controllers
            ->post( '/' , [ $this , 'cpostAction' ] )
            ->bind( 'post_students' )
        ;

        $controllers
            ->get( '/new' , [ $this , 'newAction' ] )
            ->bind( 'new_student' )
        ;

        $controllers
            ->get( '/{student}' , [ $this , 'getAction' ] )
            ->bind( 'get_student' )
        ;

        $controllers
            ->put( '/{student}' , [ $this , 'putAction' ] )
            ->bind( 'put_student' )
        ;

        $controllers
            ->delete( '/{student}' , [ $this , 'deleteAction' ] )
            ->bind( 'delete_student' )
        ;

        $controllers
            ->get( '/{student}/edit' , [ $this , 'editAction' ] )
            ->bind( 'edit_student' )
        ;

        $controllers
            ->get( '/{student}/remove' , [ $this , 'removeAction' ] )
            ->bind( 'remove_student' )
        ;

        return $controllers;
    }
}