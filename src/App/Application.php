<?php

namespace App;

use App\Controller\SecurityController;
use Symfony\Component\Security\Core\User\ChainUserProvider ,
    Symfony\Component\Security\Core\User\InMemoryUserProvider;

class Application extends \Core\Application
{
    public function boot()
    {
        if( $this['debug'] )
        {
            $this->initWhoops();
        }

        $this->initTwig();
        $this->initLocale( $this['locale'] );
        $this->initTranslation( $this['locale'] );
        $this->initTranslationYaml([ 'en' , 'cs' ]);
        $this->initHttpCache();
        $this->initNativeSession();
        $this->initValidator();
        $this->initForm( false );
        $this->initSwiftGmailer( $this['mailer_user'] , $this['mailer_pass'] );
        $this->initDoctrine( $this['mysql_host'] , $this['mysql_db'] , $this['mysql_user'] , $this['mysql_pass'] );
        $this->initDoctrineOrm( $this['mysql_db'] );

        $this->initSecurity();

        //^/.*

        parent::boot();

        $securityCtrl   =   new SecurityController( $this );
        $this->mount( '' , $securityCtrl() );
    }

    protected function initSecurity( array $security = array() )
    {
        //je potřeba, aby byl uživatel přihlášený napříč celou aplikací, ale zároveň část aplikace byla dostupná i bez přihlášení

        //routy se zabezpečí buď tak, že se nastaví url prefix v security.access_rules
        //nebo při definování routy v Controller::connect (či kdekoliv jinde) na routě zavoláme Route::secure (viz SecuredController)

        $security   =
            [
                'security.firewalls' =>
                    [
                        'admin' => array(
                            'pattern' => '^/.*',
                            'anonymous' =>  true ,
                            'form' => array('login_path' => '/login', 'check_path' => '/login_check' , 'use_referer' => true , 'default_target_path' => '/index' ),
                            'logout' => array( 'logout_path' => '/logout' , 'target_url' => '/login' ),
                            'switch_user' => array('parameter' => '_switch_user', 'role' => 'ROLE_ALLOWED_TO_SWITCH'),
                            'users' => function ()
                            {
                                $entityProv    =   new EntityUserProvider( $this['doctrine'] , 'App\Entity\User' );
                                $inMemoryProv  =   new InMemoryUserProvider(
                                    [
                                        'iddqd' =>  [ 'roles' => [ 'ROLE_IDDQD' ] , 'password' => $this['admin_password'] ] ,
                                    ] );

                                return new ChainUserProvider([ $inMemoryProv , $entityProv ]);
                            },
                        ),
                    ] ,
                'security.role_hierarchy' =>
                    [
                        'ROLE_ADMIN'    =>  [ 'ROLE_USER', 'ROLE_ALLOWED_TO_SWITCH' ] ,
                        'ROLE_IDDQD'    =>  [ 'ROLE_ADMIN' ] ,
                    ] ,
                'security.access_rules' =>
                    [
                        [ '^/admin', 'ROLE_ADMIN', 'https' ] ,
                        [ '^/students', 'ROLE_ADMIN', 'https' ] ,
                        [ '^/login' , 'IS_AUTHENTICATED_ANONYMOUSLY' , 'https' ] ,
                        [ '^/login_check' , 'IS_AUTHENTICATED_ANONYMOUSLY' , 'https' ] ,
                        [ '^/.*' , 'IS_AUTHENTICATED_ANONYMOUSLY' , 'https' ] ,
                    ] ,
            ];

        return parent::initSecurity( $security );
    }

}