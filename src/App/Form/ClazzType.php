<?php

namespace App\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type as FormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints;
use App\Entity\Student;
use App\Entity\Clazz;
use App\Entity\Teacher;

class ClazzType extends AbstractType
{
    public function buildForm( FormBuilderInterface $builder , array $options )
    {
        $builder
            ->add( 'name' , FormType\TextType::class ,
                [
                    'label'         =>  'Název' ,
                    'required'      =>  true ,
                    'constraints'   =>
                        [
                            new Constraints\Length([ 'min' => 2 , 'max' => 5 ]) ,
                            new Constraints\NotBlank() ,
                        ]
                ])
            ->add( 'Students' , EntityType::class ,
                [
                    'label'         =>  'Studenti' ,
                    'required'      =>  false ,
                    'class'         =>  Student::class ,
                    'em'            =>  $options['em'] ,
                    'query_builder' => function( EntityRepository $er ) use( $options )
                    {
                        $qb =   $er->createQueryBuilder( 's' );
                        $qb
                            ->where( 's.Clazz IS NULL' )
                            ->orderBy( 's.lastName', 'ASC' )
                        ;

                        if( $options['clazz_id'] )
                        {
                            $qb->orWhere( $qb->expr()->eq( 's.Clazz' , $options['clazz_id'] ) );
                        }

                        return $qb;
                    },
                    'empty_data'    =>  null ,
                    'multiple'      =>  true ,
                    'constraints'   =>
                        [
                            new Constraints\Valid() ,
                        ]
                ])
            ->add( 'Teachers' , EntityType::class ,
                [
                    'label'         =>  'Učitelé' ,
                    'required'      =>  false ,
                    'class'         =>  Teacher::class ,
                    'em'            =>  $options['em'] ,
                    'empty_data'    =>  null ,
                    'multiple'      =>  true ,
                    'expanded'      =>  true ,
                    'constraints'   =>
                        [
                            new Constraints\Valid() ,
                        ]
                ])
        ;
    }

    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults(array(
            'data_class'    =>  Clazz::class,
            'clazz_id'      =>  null ,
        ));

        $resolver->setRequired([ 'em' ]);
    }

}
