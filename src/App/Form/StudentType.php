<?php

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type as FormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints;
use App\Entity\Student;
use App\Entity\Clazz;

class StudentType extends AbstractType
{
    public function buildForm( FormBuilderInterface $builder , array $options )
    {
        $builder
            ->add( 'firstName' , FormType\TextType::class ,
                [
                    'label'         =>  'Jméno' ,
                    'required'      =>  true ,
                    'constraints'   =>
                        [
                            new Constraints\Length([ 'min' => 3 , 'max' => 10 ]) ,
                            new Constraints\NotBlank() ,
                        ]
                ])
            ->add( 'lastName' , FormType\TextType::class ,
                [
                    'label'         =>  'Příjmení' ,
                    'required'      =>  true ,
                    'constraints'   =>
                        [
                            new Constraints\Length([ 'min' => 3 , 'max' => 10 ]) ,
                            new Constraints\NotBlank() ,
                        ]
                ])
            ->add( 'Clazz' , EntityType::class ,
                [
                    'label'         =>  'Třída' ,
                    'required'      =>  false ,
                    'class'         =>  Clazz::class ,
                    'em'            =>  $options['em'] ,
                    'choice_label'  =>  'name' ,
                    'empty_data'    =>  null ,
                    'placeholder'   =>  '-' ,
                    'constraints'   =>
                        [
                            new Constraints\Valid() ,
                        ]
                ])
            ->add( 'StudentInfo' , StudentInfoType::class ,
                [
                    'label'         =>  'Info' ,
                    'required'      =>  false ,
                    'constraints'   =>
                        [
                            new Constraints\Valid() ,
                        ]
                ])
        ;
    }

    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults(array(
            'data_class' => Student::class,
        ));

        $resolver->setRequired([ 'em' ]);
    }

}
