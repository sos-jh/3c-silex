<?php

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type as FormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints;
use App\Entity\StudentInfo;

class StudentInfoType extends AbstractType
{
    public function buildForm( FormBuilderInterface $builder , array $options )
    {
        $builder
            ->add( 'email' , FormType\EmailType::class ,
                [
                    'label'         =>  'Email' ,
                    'required'      =>  false ,
                    'constraints'   =>
                        [
                            new Constraints\Email([ 'checkMX' => true ]) ,
                        ]
                ])
            ->add( 'phone' , FormType\TextType::class ,
                [
                    'label'         =>  'Telefon' ,
                    'required'      =>  false ,
                    'constraints'   =>
                        [
                            new Constraints\Length([ 'min' => 9 , 'max' => 9 ]) ,
                        ]
                ])
        ;
    }

    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults(array(
            'data_class' => StudentInfo::class,
        ));
    }

}
