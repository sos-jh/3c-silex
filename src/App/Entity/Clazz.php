<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Clazz
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var Collection
     */
    private $Students;
    /**
     * @var Collection
     */
    private $Teachers;

    public function __construct()
    {
        $this->Students =   new ArrayCollection();
        $this->Teachers =   new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName( $name )
    {
        $this->name = $name;
    }

    /**
     * @return Collection
     */
    public function getStudents()
    {
        return $this->Students;
    }

    /**
     * @return Collection
     */
    public function getTeachers()
    {
        return $this->Teachers;
    }

}