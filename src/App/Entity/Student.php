<?php

namespace App\Entity;

class Student
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $firstName;
    /**
     * @var string
     */
    private $lastName;
    /**
     * @var StudentInfo
     */
    private $StudentInfo;
    /**
     * @var Clazz
     */
    private $Clazz;

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName( $firstName )
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName( $lastName )
    {
        $this->lastName = $lastName;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return StudentInfo
     */
    public function getStudentInfo()
    {
        return $this->StudentInfo;
    }

    /**
     * @param StudentInfo $StudentInfo
     */
    public function setStudentInfo( $StudentInfo )
    {
        $this->StudentInfo = $StudentInfo;
    }

    /**
     * @return Clazz
     */
    public function getClazz()
    {
        return $this->Clazz;
    }

    /**
     * @param Clazz $Clazz
     */
    public function setClazz( $Clazz )
    {
        $this->Clazz = $Clazz;
    }

    public function __toString()
    {
        return $this->getLastName() .' '. $this->getFirstName();
    }

}