<?php

namespace App\Entity;

class StudentInfo
{
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $phone;
    /**
     * @var Student
     */
    private $Student;

    /**
     * @return Student
     */
    public function getStudent()
    {
        return $this->Student;
    }

    /**
     * @param Student $Student
     */
    public function setStudent( Student $Student )
    {
        $this->Student = $Student;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail( $email )
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone( $phone )
    {
        $this->phone = $phone;
    }

    public function getId()
    {
        return $this->Student->getId();
    }
}