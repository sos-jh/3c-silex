<?php

// web/index.php
require_once __DIR__.'/../vendor/autoload.php';
define( 'DIR' , __DIR__.'/../' );

$app    =   new \App\Application([
    'response_ttl'      =>  3600 ,
    'debug'             =>  true ,
    'locale'            =>  'en' ,

    'mailer_user'       =>  'robot45476578@gmail.com' ,
    'mailer_pass'       =>  'robot45476578_' ,

    'mysql_host'        =>  'localhost' ,
    'mysql_user'        =>  'root' ,
    'mysql_pass'        =>  '' ,
    'mysql_db'          =>  'school' ,

    'admin_password'    =>  'idkfa' ,
]);

$mc     =   new \App\Controller\MailController( $app );
$app->mount( '' , $mc() );

$sc     =   new \App\Controller\StudentController( $app );
$app->mount( '/students' , $sc() );

$cc     =   new \App\Controller\ClazzController( $app );
$app->mount( '/classes' , $cc() );

$app->run();
